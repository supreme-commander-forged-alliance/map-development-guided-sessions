
# Painting and blending

The goal of this part is to discuss how we can paint upon our terrain. We'll briefly look at what we have before diving into World Machine to assist us in the matter. We'll start off with a useful (prepared) template that for example generates a mask for all steep-isch terrain. Then we'll dive back into the Ozone editor and handle all the more generic terrain by hand, discussing various techniques that we can apply to make it feel uniform.

 - <INTRODUCTION>
 - Look into features of the terrain that we can exploit                                                        (10 - 15 minutes, slides)
 - Look into the idea of 'color guiding' a player through your map                                              (10 - 15 minutes, slides)

 - Dive into World Machine:
   - Look into the basics of the UI                                                                             (10 - 15 minutes)
   - Look into the basics of generating via nodes                                                               (10 - 15 minutes)
   - Add in the heightmap of our map, tune it properly                                                          (10 - 15 minutes)
   - Look into the template provided, tuning it                                                                 (20 - 25 minutes)
   - Export / import a few times to see what happens                                                            (10 - 10 minutes)
 - <BREAK / WORKING>                                                                                            (30 - 30 minutes)

 - Dive back into Ozone:
   - Look into being 'consistent' with where you put patches of something                                       (10 - 15 minutes)
   - Use decals to help adding in the boundaries of patches                                                     (20 - 25 minutes)
   - Look into 'color blending'                                                                                 (20 - 25 minutes, satalite images)
   - Look into further sculpting the terrain                                                                    (10 - 15 minutes)
 - <BREAK / WORKING>                                                                                            (45 - 45 minutes)

 - Case studies
   - Aalhaven                                                                                                   (10 - 10 minutes)
   - Example Survival Map (name: todo)                                                                          (10 - 10 minutes)
   - Other map 1                                                                                                (10 - 10 minutes)
   - Other map 2                                                                                                (10 - 10 minutes)

 - <OPEN ENDING>                                                                                                (00 - 90 minutes)
                                                                                                    Total time: 235 - 395 minutes

Notes:
 - Discuss useful hotkeys
 - Discuss the 'target' value of painting terrain w/c of brushes