
# Sculpting by Design
The goal of this part is that a plan matters and that it all starts with a draft. An idea of where you want to go. A concept of how the map plays out. We will not dive into the creation of such a concept, instead we'll dive into sculpting the map with a provided concept. We'll quickly sculpt the draft so that we have something to work on. Next off we'll discuss various techniques and 'gotchas' that you should pay attention to when sculpting. Over all we will discuss the following:

 - <INTRODUCTION>
 - Use a (temporary) painting layer to assist in sculpting the draft.                                           (15 - 20 minutes)
 - Find bits and pieces that require more attention, such as area's where a player will look more often.        (05 - 10 minutes)
 - <BREAK / WORKING>                                                                                            (60 - 60 minutes)

 - Discuss common pitfalls: 
   - Terrain that is too steep / high (bad for air)                                                             (05 - 05 minutes)
   - Terrain that is too noise (bad for ground units / projectiles)                                             (05 - 05 minutes)
   - Terrain that is too sloped (bad for artillery)                                                             (05 - 05 minutes)
   - Terrain that is not flat enough (bad for where you anticipate people to build)                             (05 - 05 minutes)
   - Terrain that is a 'bowl' shape (bad for projectiles)                                                       (05 - 05 minutes)
   - Terrain that is too inaccessible (too thin entrances)                                                      (05 - 05 minutes)
   - Terrain that is too flat (uninteresting / bad for gameplay)                                                (05 - 05 minutes)
 - <BREAK / WORKING>                                                                                            (30 - 30 minutes)
 
 - Discuss how we can use decals (normal-decals) to assist in shaping the terrain:
   - Mark ramps with decals                                                                                     (10 - 15 minutes)
   - Mark cliffs with decals                                                                                    (10 - 15 minutes)
   - Mark height changes with decals                                                                            (10 - 15 minutes)
 - <BREAK / WORKING>                                                                                            (60 - 60 minutes)
 
 - Case studies:
   - Archsimkats Valley (design aspect)                                                                         (10 - 10 minutes)
   - Aalhaven (aesthetical aspect)                                                                              (10 - 10 minutes)
   - Hardshield Oasis (inaccessible terrain)                                                                    (10 - 10 minutes)
   - Rainmakers (terrain that is not flat enough / too sloped)                                                  (10 - 10 minutes)
 
 - <OPEN ENDING>                                                                                                (00 - 90 minutes)
                                                                                                    Total time: 275 - 390 minutes

Notes:
 - Discuss useful hotkeys
 - Discuss the 'target' value of sculpting terrain w/c of brushes
